\documentclass[a4paper,10pt]{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}					%for advanced mathmematical formulas
\usepackage{graphicx}					%always use this package if you want to insert graphics into your report
\usepackage{fullpage}
\usepackage[fleqn]{mathtools}

\begingroup\expandafter\expandafter\expandafter\endgroup
\expandafter\ifx\csname pdfsuppresswarningpagegroup\endcsname\relax
\else
  \pdfsuppresswarningpagegroup=1\relax
\fi

%vertical spacing
%\smallskip, \medskip, \bigskip
%\hspace*{\fill}

\graphicspath{{./graphs/steepest_descent}{./graphs/conjugate_gradient/1_fletcher_reeves}{./graphs/conjugate_gradient/2_polak_ribiere}{./graphs/conjugate_gradient/3_polak_ribiere_plus}}


% Title Page
\title{\Huge{Method of Steepest Descent compared to Conjugate Gradient Methods}\\[7cm]Advanced Design 814\\[2cm]}

\author{\Large Andreas Joss\\[0.5cm]16450612}
\date{\today}

\begin{document}
\maketitle

\newpage
\section{Introduction}
This report briefly explains the \textit{method of steepest descent} and the \textit{conjugate gradient methods} which are used for this assignment. Furthermore, these algorithms are implemented in Python code. Example functions are put in place to evaluate successful operation of the methods. The results thereof are shown and discussed.  

\section{Theory}
\subsection{Method of Steepest Descent}
The \textit{method of steepest descent} falls under the category of \textit{first order methods} because it uses the gradient vector $\boldsymbol{\nabla} f(\textbf{x})$ to determine the direction in which the one-dimensional line search should occur.\bigskip

In this method, each new \textit{outer} iteration, the direction of steepest descent is used as the direction for a line search to be executed. It can be proven that the direction of steepest descent is in the opposite direction of the gradient vector, hence the gradient vector always points into the direction of steepest increase. The proof is done in [1:40-41]. The search direction is denoted by \textbf{u} and is calculated by
\begin{equation}\label{eq:1}
 \textbf{u} = \frac{-\boldsymbol{\nabla} f(\textbf{x})}{||\boldsymbol{\nabla} f(\textbf{x})||}
\end{equation}

\bigskip From Equation \ref{eq:1} it is clear that \textbf{u} is a normalized vector evaluated at point $f(\textbf{x})$. It can also be proved that each successive search direction \textbf{u} is orthogonal to the previous search direction [1:42], that is:

\begin{equation}\label{eq:2}
 \textbf{u}^{i+1^{T}}\textbf{u}^{i} = 0
\end{equation}


\bigskip The algorithm to solve an optimization problem is executed using the following two steps [1]:
\begin{enumerate}
  \item set $\textbf{u} = \frac{-\boldsymbol{\nabla} f(\textbf{x})}{||\boldsymbol{\nabla} f(\textbf{x})||}$
  \item set $\textbf{x}^{i} = \textbf{x}^{i-1} + \lambda_{i}\textbf{u}^{i}$ where $\lambda_{i}$ is such that\\[0.2cm]
  $F(\lambda_{i}) = f(\textbf{x}^{i-1} + \lambda_{i}\textbf{u}^{i}) = \underset{\lambda}{\text{minimize }} f(\textbf{x}^{i-1} + \lambda \textbf{u}^{i}) \ (\text{line search})$
  \item
  terminate if $||\textbf{x}^{i} - \textbf{x}^{i-1}|| < \epsilon_{1}$ or/and $||\boldsymbol{\nabla} f(\textbf{x}^{i})||< \epsilon_{2}$\\
  else set $i = i+1$ and repeat from Step 1.
\end{enumerate}

where $\epsilon_{1}$ and $\epsilon_{2}$ are very small positive tolerances. For this assignment it is selected such that $\epsilon_{1} = 10^{-4}$ and $\epsilon_{2} = 10^{-4}$. Any line search method can be used for step 2 (depending on the availability of second order derivatives), however the Golden Section method is chosen for the one-dimensional line search optimization. Notice that in step 2, a one-dimensional optimization problem is solved, with the only variable being $\lambda$. The variable $\lambda$ determines the maximum distance the next point could jump from the present point in space. \bigskip

The parameters $[a,b]$ for the Golden Section method are also chosen problem specific. The first boundary $a$ is always $a=0$ and $b$ defines the maximum size of $\lambda$. This implies that \textbf{u} always points in the correct direction (steepest descent) otherwise it would be necessary to allow $a<0$. If $b$ is assigned a too small value, extra iterations will be required to converge to the end solution (as the next solution is limited by the distance $b$ from the previous solution). \bigskip \bigskip


\subsection{Conjugate Gradient Methods}
Conjugate gradient methods have the property that the error, that is, the difference between the present solution with the theoretical solution, decreases in a quadratic manner. Furthermore, if the objective cost function is quadric itself with $n$ number of dimensions, then this method converges to the unique minimum by using $n$ or less outer iterations [1]. However, an extra outer iteration is required to satisfy the termination criteria which proves that a very small improvement is achieved after the $n^{\text{th}}$ iteration.\bigskip

In the case of the method of steepest descent, recall from Equation \ref{eq:2} that each successive direction $\textbf{u}^{i+1}$ is orthogonal with the previous direction $\textbf{u}^{i}$. Due to this property, the method of steepest descent performs poorly when the contours of the objective cost function are very elongated due to a zigzag path that is followed to achieve each new solution [1]. Conjugate gradient methods on the other hand, do not produce successive orthogonal search directions. For this reason, even though it might still require a long time to solve the optimization problem, conjugate gradient methods are in general known to converge much faster than the method of steepest descent. Conjugate gradient methods use a very similar algorithm as that of steepest descent, in fact it is only a modification for determining the search direction that sets them apart. Other parts of the algorithm such as the line search and termination criteria remain unchanged. The first iteration uses the unnormalized steepest descent direction, the following iterations receive the modification towards calculating the search direction. \bigskip

For the first iteration, thus $i = 0$, the search direction is identical to the definition of steepest descent, except for that the unnormalized direction is used instead of the normalized direction:

\begin{equation}\label{eq:3}
 \textbf{u}^{1} = -\boldsymbol{\nabla} f(\textbf{x}^{0})
\end{equation}

For the iterations $i = 1,2,...,n-1$, the search direction receives an extra term,

\begin{equation}\label{eq:4}
 \textbf{u}^{i+1} = -\boldsymbol{\nabla} f(\textbf{x}^{i}) + \beta_{i}\textbf{u}^{i}
\end{equation}

where as with the method of steepest descent, $\textbf{x}^{i} = \textbf{x}^{i-1} + \lambda_{i}\textbf{u}^{i}$ and $\lambda_{i}$ is obtained from a one-dimensional line search. There are three different options of interest for the definition of $\beta_{i}$. The scalar variable $\beta_{i}$ is defined either by the Fletcher-Reeves directions, Polak-Ribiere directions or the Polak-Ribiere ``plus'' directions. In general, it is known that for very large optimization problems, the latter two methods perform slightly better than the Fletcher-Reeves method.

\subsubsection{Fletcher-Reeves Directions}
\begin{equation}\label{eq:5}
 \beta^{(1)}_{i} = \frac{||\boldsymbol{\nabla} f(\textbf{x}^{i})||^{2}}{||\boldsymbol{\nabla} f(\textbf{x}^{i-1})||^{2}}
\end{equation}

\subsubsection{Polak-Ribiere Directions}
\begin{equation}\label{eq:6}
 \beta^{(2)}_{i} = \frac{(\boldsymbol{\nabla} f(\textbf{x}^{i}) - \boldsymbol{\nabla} f(\textbf{x}^{i-1}))^{T} \boldsymbol{\nabla} f(\textbf{x}^{i}) }{ ||\boldsymbol{\nabla} f(\textbf{x}^{i-1})||^{2}}
\end{equation}

\subsubsection{Polak-Ribiere ``Plus'' Directions}
\begin{equation}\label{eq:7}
 \beta^{(3)}_{i} = \text{max}(0, \beta^{(2)}_{i})
\end{equation}

From Equation \ref{eq:7} it is clear that this modification merely takes the maximum of either $0$ or $\beta^{(2)}_{i}$ thereby enforcing $\beta^{(3)}_{i} \geq 0$.

\section{Test functions}
The test functions in this section are implemented in Python code. The gradient vectors are calculated using a symbolic algebraic library known as Sympy. For each test function, the theoretical solution $\textbf{x}^{*}$ as well as a starting point $\textbf{x}^{0}$ is supplied. The sixth test function has a local minima which is also supplied as extra information.

\begin{enumerate}
 \item Rosenbrock's parabolic valley:\\[0.2cm]
 $f(\textbf{x}) = 100(x_{2}-x_{1}^{2})^{2} + (1-x_{1})^{2}$; \ 
  $\begin{array}{lr}
  \textbf{x}^{0} = \begin{bmatrix}
   -1.2\\1.0
   \end{bmatrix} \
   \textbf{x}^{*} = \begin{bmatrix}
   1\\1
   \end{bmatrix}
 \end{array}$\bigskip
  \item Quadratic function:\\[0.2cm]
  $f(\textbf{x}) = (x_{1}+2x_{2}-7)^{2} + (2x_{1}+x_{2}-5)^{2}$; \
    $\begin{array}{lr}
  \textbf{x}^{0} = \begin{bmatrix}
   0\\0
   \end{bmatrix} \
   \textbf{x}^{*} = \begin{bmatrix}
   1\\3
   \end{bmatrix}
 \end{array}$\bigskip
  \item Powell's quartic function:\\[0.2cm]
  $f(\textbf{x}) = (x_{1}+10x_{2})^{2} + 5(x_{3}-x_{4})^{2} + (x_{2}-2x_{3})^{4}+10(x_{1}-x_{4})^{4}$; \\[0.2cm]
    $\begin{array}{lr}
  \textbf{x}^{0} = \begin{bmatrix}
   3,&-1,&0,&1
   \end{bmatrix}^{T}; \
   \textbf{x}^{*} = \begin{bmatrix}
   0,&0,&0,&0
   \end{bmatrix}^{T}
 \end{array}$\bigskip
 \item Fletcher and Powell's helical valley:\\[0.2cm]
  $f(\textbf{x}) = 100\Big((x_{3}-10\theta(x_{1},x_{2}))^{2} + (\sqrt{x_{1}^{2} + x_{2}^{2}} - 1)^{2}\Big) + x_{3}^{2}$;\bigskip
\begin{flalign*}
  \text{where } 2\pi\theta(x_{1},x_{2}) &= \left\{
    \begin{array}{lc}
   \text{arctan}\frac{x_{2}}{x_{1}} & \text{if }      x_{1} > 0\\
   \pi + \text{arctan}\frac{x_{2}}{x_{1}} & \text{if }      x_{1} < 0
    \end{array}
    \right.& %This trailing &, together with flalign environment (from the \usepackage[fleqn]{mathtools}) package is very important to align the text to the left instead of in the center
 \end{flalign*}
  $\begin{array}{lc}
  \textbf{x}^{0} = \begin{bmatrix}
   -1,&0,&0
   \end{bmatrix}^{T}; \
   \textbf{x}^{*} = \begin{bmatrix}
   1,&0,&0
   \end{bmatrix}^{T}
 \end{array}$\bigskip
  \item A non-linear function of three variables:\\[0.2cm]
  $f(\textbf{x}) = \frac{1}{1+(x_{1}-x_{2})^{2}} + \sin\Big(\frac{1}{2}\pi x_{2} x_{3}\Big) + \text{e}^{-\Big(\frac{x_{1}+x_{3}}{x_{2}}-2\Big)^{2}}$; \\[0.2cm]
    $\begin{array}{lr}
  \textbf{x}^{0} = \begin{bmatrix}
   0,&1,&2
   \end{bmatrix}^{T}; \
   \textbf{x}^{*} = \begin{bmatrix}
   1,&1,&1
   \end{bmatrix}^{T}
 \end{array}$  \bigskip
 \item Freudenstein and Roth function:\\[0.2cm]
 $f(\textbf{x}) = (-13+x_{1}+((5-x_{2})x_{2}-2)x_{2})^{2} + (-29+x_{1}+((x_{2}+1)x_{2}-14)x_{2})^{2} $; \\[0.2cm]
  $\begin{array}{lr}
  \textbf{x}^{0} = \begin{bmatrix}
   0.5,&-2
   \end{bmatrix}^{T}; \
   \textbf{x}^{*} = \begin{bmatrix}
   5,&4
   \end{bmatrix}^{T}; \
   \textbf{x}_{\text{local}}^{*} = \begin{bmatrix}
   11.41 . . .,&-0.8968 . . .
   \end{bmatrix}^{T}
 \end{array}$  \bigskip
 \item Powell's badly scaled function:\\[0.2cm]
  $f(\textbf{x}) = (10 000x_{1}x_{2}-1)^{2} + (\text{e}^{-x_{1}} + \text{e}^{-x_{2}} - 1.0001)^{2}$; \\[0.2cm]
    $\begin{array}{lr}
  \textbf{x}^{0} = \begin{bmatrix}
   0,&1
   \end{bmatrix}^{T}; \
   \textbf{x}^{*} = \begin{bmatrix}
   1.098. . . \times 10^{-5},&9.106. . .
   \end{bmatrix}^{T}
 \end{array}$   \bigskip
 \item Brown's badly scaled function:\\[0.2cm]
   $f(\textbf{x}) = (x_{1}-10^{6})^{2} + (x_{2}-2 \times 10^{-6})^{2} + (x_{1}x_{2}-2)^{2} $; \\[0.2cm]
    $\begin{array}{lr}
  \textbf{x}^{0} = \begin{bmatrix}
   1,&1
   \end{bmatrix}^{T}; \
   \textbf{x}^{*} = \begin{bmatrix}
   10^{6},&2\times10^{6}
   \end{bmatrix}^{T}
 \end{array}$  \bigskip
 \item Beale's function:\\[0.2cm]
    $f(\textbf{x}) = (1.5-x_{1}(1-x_{2}))^{2} + (2.25 -x_{1}(1-x_{2}^{2}))^{2} + (2.625-x_{1}(1-x_{2}^{3}))^{2}$; \\[0.2cm]
    $\begin{array}{lr}
  \textbf{x}^{0} = \begin{bmatrix}
   1,&1
   \end{bmatrix}^{T}; \
   \textbf{x}^{*} = \begin{bmatrix}
   3,&0.5
   \end{bmatrix}^{T}
 \end{array}$ \bigskip
 \item Wood's function:\\[0.2cm]
    $f(\textbf{x}) = 100(x_{2}-x_{1}^{2})^{2} + (1-x_{1})^{2} + 90(x_{4}-x_{3}^{2})^{2} + (1-x_{3})^{2} + 10(x_{2}+x_{4}-2)^{2}+0.1(x_{2}-x_{4})^{2}$; \\[0.2cm]
    $\begin{array}{lr}
  \textbf{x}^{0} = \begin{bmatrix}
   -3,&-1,&-3,&-1
   \end{bmatrix}^{T}; \
   \textbf{x}^{*} = \begin{bmatrix}
   1,&1,&1,&1
   \end{bmatrix}^{T}
 \end{array}$  
\end{enumerate}

\section{Results}
The results are depicted using graphs which also includes various test parameters and outputs. The logarithmic scale line graphs show how the solution $f(\textbf{x})$ decreases as the number of outer loop iterations increase. The outer loop iteration are the iterations that belong to the steepest descent method or the conjugate gradient method, while the inner loop iterations resemble the iterations to complete a Golden Section optimization. The Golden Section parameters $[a,b]$ are problem specific and values are chosen according to experience of the specific problem. The selected Golden Section parameters are also displayed on each graph. As an extra output, the elapsed time in seconds, to complete an optimization problem (given a certain convergence tolerance), is also indicated on each graph.

Section 4.1 shows the results of the method of steepest descent and Section 4.2 shows the results of the conjugate gradient methods. The latter section displays the results of one test function optimized using three different variations of the conjugate gradient method to easier compare their performances.

\pdfsuppresswarningpagegroup=1
\newpage
\subsection{Method of Steepest Descent}
\begin{enumerate}
 \item Rosenbrock's parabolic valley:\\[0.2cm]
 \begin{figure}[h]
\centering
 \includegraphics[scale=0.55]{./graphs/steepest_descent/function1_2.pdf} 
 \caption{The Method of Steepest Descent evaluated by Rosenbrock's parabolic valley}
 \label{fig:alg1test1}
\end{figure}

 \item Quadratic function:\\[0.2cm]
 \begin{figure}[h]
\centering
 \includegraphics[scale=0.55]{./graphs/steepest_descent/function2_2.pdf} 
 \caption{The Method of Steepest Descent evaluated by a quadratic function}
 \label{fig:alg1test2}
\end{figure}

Figure \ref{fig:alg1test1} shows that the Steepest Descent requires 1339 outer iterations to converge close enough to the theoretical solution. Figure \ref{fig:alg1test2} shows that this algorithm is very effective when applied to quadratic functions since convergence occurs after 7 outer iterations.

\newpage
 \item Powell's quartic function:\\[0.2cm]
 \begin{figure}[h]
\centering
 \includegraphics[scale=0.55]{./graphs/steepest_descent/function3_3.pdf} 
 \caption{The Method of Steepest Descent evaluated by Powell's quartic function}
 \label{fig:alg1test3}
\end{figure}

 \item Fletcher and Powell's helical valley:\\[0.2cm]
 \begin{figure}[h]
\centering
 \includegraphics[scale=0.55]{./graphs/steepest_descent/function4_2.pdf} 
 \caption{The Method of Steepest Descent evaluated by Fletcher and Powell's helical valley}
 \label{fig:alg1test4}
\end{figure}

Figure \ref{fig:alg1test3} indicates that the method of steepest descent successfully optimizes the test function.  Figure \ref{fig:alg1test4} shows that the algorithm successfully converges within the prescribed convergence tolerances.

\newpage
 \item A non-linear function of three variables:\\[0.2cm]
 \begin{figure}[h]
\centering
 \includegraphics[scale=0.55]{./graphs/steepest_descent/function5_2.pdf} 
 \caption{The Method of Steepest Descent evaluated by a non-linear function of three variables}
 \label{fig:alg1test5}
\end{figure}

 \item Freudenstein and Roth function:\\[0.2cm]
 \begin{figure}[h]
\centering
 \includegraphics[scale=0.55]{./graphs/steepest_descent/function6_2.pdf} 
 \caption{The Method of Steepest Descent evaluated by Freudenstein and Roth function}
 \label{fig:alg1test6}
\end{figure}

From Figure \ref{fig:alg1test5} it seems that a lower optimum exists than the supplied optimum value $f(\textbf{x}^{*})=3$. It is suspected that the given theoretical solution of $\textbf{x}^{*} = [1, 1, 1]^{T}$ is incorrect or that the supplied test function is incorrectly described by [1:54]. Figure \ref{fig:alg1test6} indicates that the algorithm converges to the local minima stated in Section 3.

\newpage
 \item Powell's badly scaled function:\\[0.2cm]
 \begin{figure}[h]
\centering
 \includegraphics[scale=0.55]{./graphs/steepest_descent/function7_2.pdf} 
 \caption{The Method of Steepest Descent evaluated by Powell's badly scaled function}
 \label{fig:alg1test7}
\end{figure}

 \item Brown's badly scaled function:\\[0.2cm]
 \begin{figure}[h]
\centering
 \includegraphics[scale=0.55]{./graphs/steepest_descent/function8_2.pdf} 
 \caption{The Method of Steepest Descent evaluated by Brown's badly scaled function}
 \label{fig:alg1test8}
\end{figure}

Both Figure \ref{fig:alg1test7} and Figure \ref{fig:alg1test8} show that the badly scaled functions are very difficult for the algorithm to solve, since the convergence criteria is ``adhered'' to after only one or two outer iterations.

\newpage
 \item Beale's function:\\[0.2cm]
 \begin{figure}[h]
\centering
 \includegraphics[scale=0.55]{./graphs/steepest_descent/function9_3.pdf} 
 \caption{The Method of Steepest Descent evaluated by Beale's function}
 \label{fig:alg1test9}
\end{figure}

 \item Wood's function:\\[0.2cm]
 \begin{figure}[h]
\centering
 \includegraphics[scale=0.55]{./graphs/steepest_descent/function10_2.pdf} 
 \caption{The Method of Steepest Descent evaluated by Wood's function}
 \label{fig:alg1test10}
\end{figure}

Figure \ref{fig:alg1test9} indicates that the steepest descent method does successfully converge close to the theoretical solution. Figure \ref{fig:alg1test10} shows that this test function is also minimized to some local minima after 92 outer iterations. However, from the logarithmic scale it can be noted that the objective cost function $f(\textbf{x})$ is greatly improved from the initial starting point.

\end{enumerate}

\newpage
\subsection{Conjugate Gradient Methods}

\begin{enumerate}
 \item Rosenbrock's parabolic valley:\\[0.2cm]
 \begin{figure}[h]
\centering
 \includegraphics[scale=0.55]{./graphs/conjugate_gradient/method4function1.pdf} 
 \caption{Three conjugate gradient methods evaluated by Rosenbrock's parabolic valley}
 \label{fig:alg2test1}
\end{figure}

Figure \ref{fig:alg2test1} shows that the Fletcher-Reeves direction, and the Polak-Ribiere Plus direction converge successfully within the specified tolerance to the theoretical solution. It is interesting to see that the Polak-Ribiere Plus does provide improved accuracy compared to both the Fletcher-Reeves direction and the method of steepest descent as seen in Figure \ref{fig:alg1test1}. Except for the normal Polak-Ribiere direction which does not converge for this test function, the remaining conjugate gradient methods converge dramatically faster iteration-wise and time-wise than the method of steepest descent. 

\newpage
 \item Quadratic function:\\[0.2cm]
 \begin{figure}[h]
\centering
 \includegraphics[scale=0.55]{./graphs/conjugate_gradient/method4function2.pdf} 
 \caption{Three conjugate gradient methods evaluated by a quadratic function}
 \label{fig:alg2test2}
\end{figure}

Figure \ref{fig:alg2test2} indicates that all three conjugate gradient directions converge successfully to the theoretical solution. Notice that the Polak-Ribiere and Polak-Ribiere Plus directions produce the same solutions for this test function. Furthermore, as mentioned in Section 2.2, if the test function itself is quadratic with $n$ dimensions, then the conjugate gradient method will converge to a solution using $n$ or less outer iterations. An extra iteration is however required to satisfy the termination criteria, thus as expected for this 2-dimensional quadratic test function, Figure \ref{fig:alg2test2} shows that all three directions use $n=3$ outer iterations to converge within the specified tolerances.

\newpage
 \item Powell's quartic function:\\[0.2cm]
 \begin{figure}[h]
\centering
 \includegraphics[scale=0.55]{./graphs/conjugate_gradient/method4function3.pdf} 
 \caption{Three conjugate gradient methods evaluated by Powell's quartic function}
 \label{fig:alg2test3}
\end{figure}

Figure \ref{fig:alg2test3} shows that each of the conjugate gradient methods optimizes much faster and has increased accuracy as compared to the results obtained by the method of steepest descent in Figure \ref{fig:alg1test3}. As with Figure \ref{fig:alg2test1} and \ref{fig:alg2test2} it is again apparent that the Polak-Ribiere and Polak-Ribiere Plus directions produce slightly improved optimum solutions as compared to the Fletcher-Reeves direction. For this specific test function, the outer iterations are limited to 90 as the program struggles to perform calculations after 90 iterations.

\newpage
 \item Fletcher and Powell's helical valley:\\[0.2cm]
 \begin{figure}[h]
\centering
 \includegraphics[scale=0.55]{./graphs/conjugate_gradient/method4function4.pdf} 
 \caption{Three conjugate gradient methods evaluated by Fletcher and Powell's helical valley}
 \label{fig:alg2test4}
\end{figure}

 \begin{figure}[h]
\centering
 \includegraphics[scale=0.55]{./graphs/conjugate_gradient/method3function4.pdf} 
 \caption{Polak-Ribiere Plus direction evaluated by Fletcher and Powell's helical valley}
 \label{fig:alg2test4_1}
\end{figure}

Interestingly, the conjugate gradient method implementation, as seen in Figure \ref{fig:alg2test4} struggles far more than the method of steepest descent to converge to the solution as indicated in Figure \ref{fig:alg1test4}. In fact Figure \ref{fig:alg2test4} shows that all the conjugate gradient methods fail to converge to a solution compared to the method of steepest descent which does produce the correct solution. The outer iterations are limited to 10 outer iterations, due to the excessive time required by the Fletcher-Reeves method. For this reason, an additional graph depicting the convergence, or rather the oscillation of the most time efficient conjugate gradient method for this test function, is shown by Figure \ref{fig:alg2test4_1}.

\newpage
 \item A non-linear function of three variables:\\[0.2cm]
 \begin{figure}[h]
\centering
 \includegraphics[scale=0.55]{./graphs/conjugate_gradient/method4function5.pdf} 
 \caption{Three conjugate gradient methods evaluated by a non-linear function of three variables}
 \label{fig:alg2test5}
\end{figure}

As previously mentioned in Section 4.1, it is suspected that either the incorrect theoretical solution is given by [1:54] or that the test function is incorrectly described. The solutions obtained by Figure \ref{fig:alg1test5} and Figure \ref{fig:alg2test5} suggest that the actual solution could be $f(\textbf{x}^{*}) = -1$ as both the method of steepest descent and the conjugate gradient method seem to converge to this value. Notice that the conjugate gradient methods require much less time and fewer outer iterations to generate the solution as compared to the solution given by the steepest descent method in Figure \ref{fig:alg1test5}. Once again it also seems that the Polak-Ribiere Plus method performs more effectively than the Polak-Ribiere method, and the Polak-Ribiere method performs more effectively than the Fletcher-Reeves direction.

\newpage
 \item Freudenstein and Roth function:\\[0.2cm]
 \begin{figure}[h]
\centering
 \includegraphics[scale=0.55]{./graphs/conjugate_gradient/method4function6.pdf} 
 \caption{Three conjugate gradient methods evaluated by Freudenstein and Roth function}
 \label{fig:alg2test6}
\end{figure}

As shown by Figure \ref{fig:alg2test6}, the Fletcher-Reeves direction does not produce the desired optimum value, but rather some local minimum. The Polak-Ribiere and Polak-Ribiere Plus directions however do produce the correct solutions within a very short period of time. Notice that in this case it seems that the Polak-Ribiere Plus method is computationally more expensive than the Polak-Ribiere direction, but could generate more optimum values than the Polak-Ribiere direction as seen with the previous examples. Also note that, as previously shown by Figure \ref{fig:alg1test6}, the method of steepest descent only converges to the local minimum, which is in fact better than the solution that the Fletcher-Reeves direction produces.

\newpage
 \item Powell's badly scaled function:\\[0.2cm]
 \begin{figure}[h]
\centering
 \includegraphics[scale=0.55]{./graphs/conjugate_gradient/method4function7.pdf} 
 \caption{Three conjugate gradient methods evaluated by Powell's badly scaled function}
 \label{fig:alg2test7}
\end{figure}

It is clear from Figure \ref{fig:alg2test7} that the badly scaled test function is very difficult to optimize, as is found by the method of steepest descent and shown by Figure \ref{fig:alg1test7}. With the conjugate gradient methods, it is interesting to see that the solution actually diverges as compared to the method of steepest descent where the solution greatly remains unchanged.\bigskip

\item Brown's badly scaled function:

No output graphs are obtainable from the conjugate gradient implementation. Again as with the previous test function, this points out the difficulty of optimizing a badly scaled test function.

%  \item Brown's badly scaled function:\\[0.2cm]
%  \begin{figure}[h]
% \centering
%  \includegraphics[scale=0.55]{./graphs/conjugate_gradient/method4function7.pdf} 
%  \caption{Three conjugate gradient methods evaluated by Brown's badly scaled function}
%  \label{fig:alg2test8}
% \end{figure}

\newpage
 \item Beale's function:\\[0.2cm]
 \begin{figure}[h]
\centering
 \includegraphics[scale=0.55]{./graphs/conjugate_gradient/method4function9.pdf} 
 \caption{Three conjugate gradient methods evaluated by Beale's function}
 \label{fig:alg2test9}
\end{figure}

From Figure \ref{fig:alg2test9} it is clear that all three conjugate gradient method variations successfully converge to the correct solution. As frequently seen by the previous test function results, the Polak-Ribiere and Polak-Ribiere Plus directions are less time demanding and produce slightly improved optimum values compared to the Fletcher-Reeves direction. However all three conjugate gradient variations produce more optimum results and also within a shorter time frame as seen by Figure \ref{fig:alg1test7}.

\newpage
 \item Wood's function:\\[0.2cm]
 \begin{figure}[h]
\centering
 \includegraphics[scale=0.55]{./graphs/conjugate_gradient/method4function10.pdf} 
 \caption{Three conjugate gradient methods evaluated by Wood's function}
 \label{fig:alg2test10}
\end{figure}

Figure \ref{fig:alg2test10} shows that only the Polak-Ribiere direction generates a solution very close to the theoretical solution. The Polak-Ribiere Plus graph seems to oscillate slightly before converging to a local minima. This local minima seems to be at $\textbf{x}_{local} = [-1, 1, -1, 1]^{T}$ which is also indicated by Figure \ref{fig:alg1test10}.

\end{enumerate}
\section{Conclusion}
This report evaluated 10 mathematical test functions on both the method of steepest descent and the conjugate gradient methods. The results of the two methods were compared, as well as the different variations of conjugate gradient methods were compared. The three variations of the conjugate gradient method implemented in this assignment were Fletcher-Reeves, Polak-Ribiere and Polak-Ribiere Plus.\bigskip

Most test functions clearly showed that conjugate gradient methods were computationally less expensive and provided slightly more optimum solutions than that of the method of steepest descent. However it was also shown that this is not always true, and sometimes the steepest descent method converges to the theoretical solution when the conjugate gradient methods were unsuccessful to converge (as seen by Figures \ref{fig:alg1test4}, \ref{fig:alg2test4} and \ref{fig:alg2test4_1}).\bigskip

Furthermore if only the conjugate gradient method results from Section 4.2 are considered, it was found that in general the Polak-Ribiere Plus provided the most optimum values except for the results shown by Figure \ref{fig:alg2test10}. However, the regular Polak-Ribiere direction proved to be more time efficient. In general both the Polak-Ribiere and Polak-Ribiere Plus directions performed slightly better than the Fletcher-Reeves method, with the only exception depicted by Figure \ref{fig:alg2test4}.\bigskip

To conclude, it seems that from the four different optimization tools implemented in this assignment, the Polak-Ribiere and Polak-Ribiere Plus are likely to produce the best results, however this is not always true and therefore the remaining methods, namely the method of steepest descent and the Fletcher-Reeves direction, are indeed handy for verification and to serve as back-up optimization tools.

\section{Bibliography}
1. Snyman J. Practical Mathmematical Optimization. Technical report, Department of Mechanical and Aeronautical Engineering University of Pretoria, Pretoria, South Africa, 2005.

\end{document} 
