#!/usr/bin/python

#Author:	Andreas Joss (16450612)
#Date:		2015/03/16 

###STEEPEST DESCENT ALGORITHM
#outlay of this algorithm:
#1 ten test functions are to be tested as found on p53-55 of "Practical Mathematical Optimization" textbook
#2 any number of iterations can be specified
#3 determine direction of steepest descent
#4 do a line search in the direction of steepest descent
#5 terminate program when one of two criteria are met

from __future__ import division				#ensures division always returns the correct answer (eg. 1/2 = 0.5 instead of 1/2 = 0)
#from __future__ import print_function			#The Python 3 print() function is already in the builtins, but cannot be accessed from Python 2 code unless you use the appropriate future statement
import argparse
import pylab as pl
from sympy import *
import mpmath as mp					#use this for mathmematical constants
import time
import sys

start_time = time.time()

###GLOBAL VARIABLES
##########################################################################################
example_string = []
f_graph = []
epsilon1 = 1e-4			#10^(-4)	1st termination condition of steepest descent (outer loop)
epsilon2 = 1e-4			#10^(-4)	2nd termination condition of steepest descent (outer loop)
epsilon_golden_section = 1e-6	#10^(-6)	termination condition of inner loop
r = 0.618034			#golden ratio
golden = 1/r			#golden ratio reciprocal (used for initial bracketing)
inner_iterations = 0
x_star = ""
x_start = ""
f_star = 0

x_sol = []
gradient_vector_at_point = []

example_string.append("1 -  Rosenbrock's parabolic valley")
example_string.append("2 -  Quadratic function")
example_string.append("3 -  Powell's quartic function")
example_string.append("4 -  Fletcher and Powell's helical valley")
example_string.append("5 -  A non-linear function of three variables")
example_string.append("6 -  Freudenstein and Roth function")
example_string.append("7 -  Powell's badly scaled function")
example_string.append("8 -  Brown's badly scaled function")
example_string.append("9 -  Beale's function")
example_string.append("10 - Wood's function")

##########################################################################################


###PARSE INFORMATION
##########################################################################################
parser = argparse.ArgumentParser(description="Steepest Descent",formatter_class=argparse.RawTextHelpFormatter)

parser.add_argument("choice", help="Select example function to test the Steepest Descent Algorithm on:\n"+
		    example_string[0] + "\n"+
		    example_string[1] + "\n"+
		    example_string[2] + "\n"+
		    example_string[3] + "\n"+
		    example_string[4] + "\n"+
		    example_string[5] + "\n"+
		    example_string[6] + "\n"+
		    example_string[7] + "\n"+
		    example_string[8] + "\n"+
		    example_string[9] + "\n"
		    ,type=int, choices=[0,1,2,3,4,5,6,7,8,9,10])					#add a new POSITIONAL argument with a help message included

parser.add_argument("max_outer", help="Enter maximum number of outer iterations allowed", type=int)			#add a new POSITIONAL argument with a help message included

args = parser.parse_args()									#store the argument parsed into the variable "args"

##########################################################################################


###OBTAIN TEST_PARAMETERS
##########################################################################################
#0 Very easy 2D function for debugging
if args.choice == 0:
  x_sol.append(pl.np.array([1,1]))
  x_star_num = [0,0]
  x_star = "[0,0]"
  x_start = "[1,1]"
  x = []
  n_dim = pl.np.size(x_sol)
  
  for i in range (0,n_dim+1):
    x.append(0)
    
  for i in range (0,n_dim):
    gradient_vector_at_point.append(0)
  
  x[1] = Symbol('x[1]')
  x[2] = Symbol('x[2]')
  f = Function('f')(x[1],x[2])
  
  f = x[1]**2 + 3*x[2]**2
  
  gradient_vector = [f.diff(x[1],1) , f.diff(x[2],1)]		###THIS WORKS CORRECTLY###

#1 Test function 1
if args.choice == 1:
  A = 0
  B = 1
  x_sol.append(pl.np.array([-1.2,1.0]))
  x_star_num = [1,1]
  x_star = "[1,1]"
  x_start = "[-1.2,1.0]"
  x = []
  n_dim = pl.np.size(x_sol)
  
  for i in range (0,n_dim+1):
    x.append(0)
    
  for i in range (0,n_dim):
    gradient_vector_at_point.append(0)
  
  x[1] = Symbol('x[1]')
  x[2] = Symbol('x[2]')
  f = Function('f')(x[1],x[2])
  
  f = 100*(x[2] - x[1]**2)**2 + (1-x[1])**2
  
  gradient_vector = [f.diff(x[1],1) , f.diff(x[2],1)]

#2 Test function 2
elif args.choice == 2:
  A = 0
  B = 4  
  x_sol.append(pl.np.array([0,0]))
  x_star_num = [1,3]
  x_star = "[1,3]"
  x_start = "[0,0]"
  x = []
  n_dim = pl.np.size(x_sol)
  
  for i in range (0,n_dim+1):
    x.append(0)
    
  for i in range (0,n_dim):
    gradient_vector_at_point.append(0)
  
  x[1] = Symbol('x[1]')
  x[2] = Symbol('x[2]')
  f = Function('f')(x[1],x[2])
  
  f = (x[1] + 2*x[2] - 7)**2 + (2*x[1] + x[2] - 5)**2
  
  gradient_vector = [f.diff(x[1],1) , f.diff(x[2],1)]

#3 Test function 3
elif args.choice == 3:
  A = 0
  B = 2
  x_sol.append(pl.np.array([3,-1,0,1]))
  x_star_num = [0,0,0,0]
  x_star = "[0,0,0,0]"
  x_start = "[3,-1,0,1]"
  x = []
  n_dim = pl.np.size(x_sol)
  
  for i in range (0,n_dim+1):
    x.append(0)
    
  for i in range (0,n_dim):
    gradient_vector_at_point.append(0)
  
  x[1] = Symbol('x[1]')
  x[2] = Symbol('x[2]')
  x[3] = Symbol('x[3]')
  x[4] = Symbol('x[4]')  
  f = Function('f')(x[1],x[2],x[3],x[4])
  
  f = (x[1] + 10*x[2])**2 + 5*(x[3]-x[4])**2 + (x[2]-2*x[3])**4 + 10*(x[1]-x[4])**4
  
  gradient_vector = [f.diff(x[1],1) , f.diff(x[2],1), f.diff(x[3],1) , f.diff(x[4],1)]
  
#4 Test function 4
elif args.choice == 4:
  A = 0
  B = 1
  x_sol.append(pl.np.array([-1,0,0]))
  x_star_num = [1,0,0]
  x_star = "[1,0,0]"
  x_start = "[-1,0,0]"
  x = []
  n_dim = pl.np.size(x_sol)
  
  for i in range (0,n_dim+1):
    x.append(0)
    
  for i in range (0,n_dim):
    gradient_vector_at_point.append(0)
  
  x[1] = Symbol('x[1]')
  x[2] = Symbol('x[2]')
  x[3] = Symbol('x[3]')
  
  
  #theta = Function('theta')(x[1],x[2])
  f = Function('f')(x[1],x[2],x[3])
  
  theta = Piecewise( ((atan(x[2]/x[1])/(2*pi)), x[1]>0), ((pi + atan(x[2]/x[1])/(2*pi)), x[1]<0))
  
  f = 100*((x[3]-10*theta)**2 + ((x[1]**2 + x[2]**2)**Rational(1,2) - 1)**2) + x[3]**2 
  
  gradient_vector = [f.diff(x[1],1) , f.diff(x[2],1), f.diff(x[3],1)]
  
#5 Test function 5
elif args.choice == 5:
  A = 0
  B = 1
  x_sol.append(pl.np.array([0,1,2]))
  x_star_num = [1,1,1]
  x_star = "[1,1,1]"
  x_start = "[0,1,2]"
  x = []
  n_dim = pl.np.size(x_sol)
  
  for i in range (0,n_dim+1):
    x.append(0)
    
  for i in range (0,n_dim):
    gradient_vector_at_point.append(0)
  
  x[1] = Symbol('x[1]')
  x[2] = Symbol('x[2]')
  x[3] = Symbol('x[3]')  
  f = Function('f')(x[1],x[2],x[3])
  
  f = 1/(1+(x[1]-x[2])**2) + sin(0.5*pi*x[2]*x[3]) + mp.e**(-((x[1]+x[3])/x[2] - 2)**2)
  
  gradient_vector = [f.diff(x[1],1) , f.diff(x[2],1), f.diff(x[3],1)]
  
#6 Test function 6
elif args.choice == 6:
  A = 0
  B = 5
  x_sol.append(pl.np.array([0.5,-2]))
  x_star_num = [5,4]
  x_star = "[5,4]"
  x_start = "[0.5,-2]"
  x = []
  n_dim = pl.np.size(x_sol)
  
  for i in range (0,n_dim+1):
    x.append(0)
    
  for i in range (0,n_dim):
    gradient_vector_at_point.append(0)
  
  x[1] = Symbol('x[1]')
  x[2] = Symbol('x[2]')
  f = Function('f')(x[1],x[2])
  
  f = (-13 + x[1] + ((5 - x[2])*x[2] - 2)*x[2])**2 + (-29 + x[1] + ((x[2] + 1)*x[2] - 14)*x[2])**2
  
  gradient_vector = [f.diff(x[1],1) , f.diff(x[2],1)]  
  
#7 Test function 7
elif args.choice == 7:
  A = 0
  B = 2
  x_sol.append(pl.np.array([0,1]))
  x_star_num = [1.098*1e-5,9.106]
  x_star = "[1.098*1e-5,9.106]"
  x_start = "[0,1]"
  x = []
  n_dim = pl.np.size(x_sol)
  
  for i in range (0,n_dim+1):
    x.append(0)
    
  for i in range (0,n_dim):
    gradient_vector_at_point.append(0)
  
  x[1] = Symbol('x[1]')
  x[2] = Symbol('x[2]')
  f = Function('f')(x[1],x[2])
  
  f = (10000*x[1]*x[2] - 1)**2 + (mp.e**(-x[1]) + mp.e**(-x[2]) - 1.0001)**2
  
  gradient_vector = [f.diff(x[1],1) , f.diff(x[2],1)]    
  
#8 Test function 8
elif args.choice == 8:
  A = 0
  B = 2
  x_sol.append(pl.np.array([1,1]))
  x_star_num = [1e-6,2*1e-6]
  x_star = "[1e-6,2*1e-6]"
  x_start = "[1,1]"
  x = []
  n_dim = pl.np.size(x_sol)
  
  for i in range (0,n_dim+1):
    x.append(0)
    
  for i in range (0,n_dim):
    gradient_vector_at_point.append(0)
  
  x[1] = Symbol('x[1]')
  x[2] = Symbol('x[2]')
  f = Function('f')(x[1],x[2])
  
  f = (x[1]-1e-6)**2 + (x[2] - 2e-6)**2 + (x[1]*x[2] -2)**2
  
  gradient_vector = [f.diff(x[1],1) , f.diff(x[2],1)]  
  
#9 Test function 9
elif args.choice == 9:
  A = 0
  B = 2
  x_sol.append(pl.np.array([1,1]))
  x_star_num = [3,0.5]
  x_star = "[3,0.5]"
  x_start = "[1,1]"
  x = []
  n_dim = pl.np.size(x_sol)
  
  for i in range (0,n_dim+1):
    x.append(0)
    
  for i in range (0,n_dim):
    gradient_vector_at_point.append(0)
  
  x[1] = Symbol('x[1]')
  x[2] = Symbol('x[2]')
  f = Function('f')(x[1],x[2])
  
  f = (1.5 - x[1]*(1 - x[2]))**2 + (2.25 - x[1]*(1 - x[2]**2))**2 + (2.625 - x[1]*(1 - x[2]**3))**2
  
  gradient_vector = [f.diff(x[1],1) , f.diff(x[2],1)]  
  
#10 Test function 10
elif args.choice == 10:
  A = 0
  B = 2
  x_sol.append(pl.np.array([-3,-1,-3,-1]))
  x_star_num = [1,1,1,1]
  x_star = "[1,1,1,1]"
  x_start = "[-3,-1,-3,-1]"
  x = []
  n_dim = pl.np.size(x_sol)
  
  for i in range (0,n_dim+1):
    x.append(0)
    
  for i in range (0,n_dim):
    gradient_vector_at_point.append(0)
  
  x[1] = Symbol('x[1]')
  x[2] = Symbol('x[2]')
  x[3] = Symbol('x[3]')
  x[4] = Symbol('x[4]')  
  f = Function('f')(x[1],x[2],x[3],x[4])
  
  f = 100*(x[2] - x[1]**2)**2 + (1 - x[1])**2 + 90*(x[4] - x[3]**2)**2 + (1 - x[3])**2 + 10*(x[2] + x[4] - 2)**2 + 0.1*(x[2] - x[4])**2
  
  gradient_vector = [f.diff(x[1],1) , f.diff(x[2],1), f.diff(x[3],1) , f.diff(x[4],1)]  

##########################################################################################


###HELPER FUNCTIONS
##########################################################################################

# Obtain absolute value of a gradient vector evaulated at a point
def absolute_of_first_order_vector_at_point(sol_vector):	###THIS FUNCTION WORKS###
  sum = 0
  k = 0
  j = 0
  
  #create new list from scratch (so that modification of this list does not affect the original list contents)
  element_of_gradient_vector_evaulated_at_sol_vector = []
  for j in range (0,n_dim):
    element_of_gradient_vector_evaulated_at_sol_vector.append(0)
  
  for j in range (0,n_dim):
    element_of_gradient_vector_evaulated_at_sol_vector[j] = gradient_vector[j] 	#copy element by element into new variable, otherwise only pointer address is copied and changes will affect both variables (undesired)
    for k in range (0,n_dim):
      element_of_gradient_vector_evaulated_at_sol_vector[j] = element_of_gradient_vector_evaulated_at_sol_vector[j].subs(x[k+1],sol_vector[k])		#can only substitute one variable at a time

    sum = sum + pl.np.power(element_of_gradient_vector_evaulated_at_sol_vector[j],2)   

  return sum**Rational(1,2)


### Obtain absolute value of a vector
def absolute_of_vector(vector):					###THIS FUNCTION WORKS###
  sum = 0
  for j in range (0,n_dim):
    sum = sum + pl.np.power(vector[j],2)
  
  return sum**Rational(1,2)			#must use sympy's rational function because sum contains a sympy_float object type



### Substitute vector position into function g and return the evaluated function
def substitute_vector_into_function(vector,g):
  n_size = pl.np.size(vector)
  
  for j in range (0,n_size):
    for k in range (0,n_size):
      evaluated_function[j] = g[j].subs(x[k+1],vector[k])		#can only substitute one variable at a time

  return evaluated_function



### Initial bracketing of minimum
def initialBracketing(x_i_old):					###NOT PRIORITY NOW###
  delta = 0.1			#delta should be a small number delta > 0
  
  #a < b < c			#triplet of points the minimum should lie within
  a = 0
  b = delta
  
  x_i_a = x_i_old + a*u		#remember x_i_a is a vector
  x_i_b = x_i_old + b*u
  
  #f_a = 
  
  #if (x_i_a < x_i_old): 
  
  
  #return max_lambda	#lambda can only assume positive values because we already chose a direction "u" to minimize in.
			#max_lambda will determine how far we should search on the line [0,max_lambda] 

### Golden Section Method
def goldenSection(a,b):
  L = b - a			#Length of line, search is conducted on
  solution = pl.np.zeros((3,1))
  m = 0
  lambda1 = a + pl.np.power(r,2)*L
  lambda2 = a + r*L
  solution_found = False
  order = 0
  #lambda can only assume positive values because we a_sol[i]lready chose a direction "u" to minimize in.
  
  while(solution_found == False):
    global inner_iterations
    inner_iterations = inner_iterations + 1
    
    x1 = x_sol[i] + lambda1*u		#x1 is a vector
    x2 = x_sol[i] + lambda2*u		#x2 is a vector
    
    #This is a very important piece of code, otherwise x1 and x2 will remain a sympy object in a very large uncompressed algebraic form
    for p in range(0,n_dim):
      x1[p] = pl.np.float(x1[p])
      x2[p] = pl.np.float(x2[p])
    
    g1 = f					#f is a function of multi variables
    g2 = f

    for p in range (0,n_dim):
      g1 = g1.subs(x[p+1],x1[p])
      g2 = g2.subs(x[p+1],x2[p])	
  
    m = m + 1

    if L < epsilon_golden_section:					
      solution[0] = (b+a)/2	
      solution[2] = m
      solution_found = True
      return solution

    if(StrictGreaterThan(g1,g2)):  
      a = lambda1
      lambda1 = lambda2
      L = b - a
      lambda2 = a + r*L

    elif(StrictLessThan(g1,g2)):  
      b = lambda2
      lambda2 = lambda1
      L = b - a
      lambda1 = a + pl.np.power(r,2)*L
    
##########################################################################################


###STEEPEST DESCENT
##########################################################################################
i=0
solution_found = False

f1 = f
f_star = f
for p in range (0,n_dim):
  f1 = f1.subs(x[p+1],x_sol[i][p])
  f_star = f_star.subs(x[p+1],x_star_num[p])

f_star = pl.np.float(f_star)
f1 = pl.np.float(f1)  
f_graph.append(f1)

while (solution_found == False):

  #get denominator
  denominator = absolute_of_first_order_vector_at_point(x_sol[i])			###THIS IS CORRECT VALUE###
  
  #get nominator
  #gradient_vector_at_point = gradient_vector						#CAREFUL (This only transfers the original pointer address to another variable
  for j in range (0,n_dim):								#j denotes gradient_vector_at_point row number
    gradient_vector_at_point[j] = gradient_vector[j]					#COPY element by element
    for k in range (0,n_dim):								#k denotes which variable (x[1],x[2],..) is to be substituted from the symbolic equation
      gradient_vector_at_point[j] = gradient_vector_at_point[j].subs(x[k+1],x_sol[i][k])		#can only substitute one variable at a time
  
  #get direction of steepest descent
  gradient_vector_at_point_numpy = pl.np.array(gradient_vector_at_point)		###THIS IS CORRECT VALUE###
  u = -gradient_vector_at_point_numpy/denominator					###THIS IS CORRECT VALUE###
  
  #do a line search minimization
  goldenSectionResult = goldenSection(0,B)	#A is always zero for Steepest Descent Method
  lambda_i = goldenSectionResult[0]
  
  #set new x_sol value according to the minimum found on the straight line
  i=i+1
  x_sol.append(x_sol[i-1])	#just append any value to create the next entry on list (otherwise an index out of range error will occur)
  x_sol[i] = pl.np.array(x_sol[i-1] + lambda_i*u)
  
  for p in range(0,n_dim):
    x_sol[i][p] = pl.np.float(x_sol[i][p])	#This is a very important piece of code, otherwise x_sol will remain sympy object in a very large uncompressed algebraic form 
  
  if (absolute_of_vector(x_sol[i]-x_sol[i-1]).evalf() < epsilon1) or (denominator.evalf() < epsilon2):
    solution_found = True
  
  f1 = f										#CAREFUL (This only transfers the original pointer address to another variable)
  for p in range (0,n_dim):
    f1 = f1.subs(x[p+1],x_sol[i][p])
  
  f1 = pl.np.float(f1)
  f_graph.append(f1)
  f_numpy = pl.np.array(f1)
  
  if i==args.max_outer:
    solution_found = True
  
  #print-out of present iteration without using a newline
  sys.stdout.write("%d\r" %i)
  sys.stdout.flush()
  
###OUTPUT GRAPHS
##########################################################################################

fig1 = pl.figure(1)
pl.title("f(x) vs. number of iterations executed")

pl.xlabel("Iteration number")
pl.ylabel("f(x)")
pl.yscale('log')
pl.plot(range(0,i+1),f_graph)

pl.figtext(0.50,0.882, "Example function: %s\nEpsilon1(outer): %8.6f\nEpsilon2(outer): %8.6f\nEpsilon(inner)  : %8.6f\nTheoretical solution: %s\nf(x*) = %6.4f\nStarting position: %s\nGolden Section [a,b] = [%d,%d]\nOuter iterations: %d\nInner iterations: %d\nTime elapsed: %.2f seconds" %(example_string[args.choice-1],epsilon1,epsilon2,epsilon_golden_section,x_star,f_star,x_start,A,B, i,inner_iterations,time.time()-start_time),fontsize=10, bbox=dict(facecolor = 'white', alpha=1, linewidth = 1, edgecolor = 'black'), horizontalalignment = 'center',verticalalignment='top')

x_sol_string = "Final f(x) = %6.4f\n" %f_numpy
for p in range (1,n_dim+1):
  x_sol_string = x_sol_string + "\nx%d = %7.4f" %(p,x_sol[i][p-1])

pl.figtext(0.50,0.582, "Solution\n" + x_sol_string,fontsize=10, bbox=dict(facecolor = 'white', alpha=1, linewidth = 1, edgecolor = 'black'), horizontalalignment = 'center',verticalalignment='top')

l = pl.legend(loc=1)# draggable(state=True))



F = pl.gcf()
F.savefig("./graphs/steepest_descent/function%d_3.pdf" %args.choice,bbox_inches='tight',format='PDF',pad_inches=0.1)
pl.show()